function convertir() {
    var cantidad = parseFloat(document.getElementById('cantidad').value);
    var seleccion = document.querySelector('input[name="conversion"]:checked').value;
    var resultado;

    if (seleccion === "celsiusToFahrenheit") {
        resultado = (9/5 * cantidad) + 32;
    } else {
        resultado = (cantidad - 32) * 5/9;
    }

    document.getElementById('resultado').innerHTML = "Resultado: " + resultado.toFixed(2);
}

function limpiar() {
    document.getElementById('cantidad').value = "";
    document.getElementById('resultado').innerHTML = "Resultado:";
}