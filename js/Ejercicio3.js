function generarEdades() {
    const edades = [];
    for (let i = 0; i < 100; i++) {
        edades.push(Math.floor(Math.random() * 91));
    }

    let bebes = 0;
    let ninos = 0;
    let adolescentes = 0;
    let adultos = 0;
    let ancianos = 0;
    let sumaEdades = 0; // Variable para calcular la suma de edades

    for (let i = 0; i < edades.length; i++) {
        const edad = edades[i];
        sumaEdades += edad; // Sumar las edades

        if (edad >= 1 && edad <= 3) {
            bebes++;
        } else if (edad >= 4 && edad <= 12) {
            ninos++;
        } else if (edad >= 13 && edad <= 17) {
            adolescentes++;
        } else if (edad >= 18 && edad <= 60) {
            adultos++;
        } else if (edad >= 61 && edad <= 100) {
            ancianos++;
        }
    }

    const edadPromedio = sumaEdades / edades.length; // Calcular la edad promedio

    document.getElementById('edades').textContent = "Edades de las 100 personas: " + edades.join(', ');
    document.getElementById('bebes').textContent = bebes;
    document.getElementById('ninos').textContent = ninos;
    document.getElementById('adolescentes').textContent = adolescentes;
    document.getElementById('adultos').textContent = adultos;
    document.getElementById('ancianos').textContent = ancianos;
    document.getElementById('edadPromedio').textContent = edadPromedio.toFixed(2); // Mostrar la edad promedio con 2 decimales
}
